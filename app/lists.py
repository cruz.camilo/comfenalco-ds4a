import lib.db as db
import pandas as pd

escolaridad = [(escolaridad.lower()).title() for escolaridad in list(db.df_escolaridad['dsescolaridad'])]
escolaridad_id = list(db.df_escolaridad['dsescolaridad'])
municipios = db.municipios_list
municipios_id = list(db.df_municipios['dsmunicipio'])