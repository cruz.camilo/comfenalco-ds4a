# Basics Requirements
from lib import recommendation_form, student_form, course_form, plots_methods, db, matching, recommendation_cards

import pathlib
import dash
from dash.dependencies import Input, Output, State, ClientsideFunction
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import plotly.graph_objects as go
import dash_bootstrap_components as dbc
import json
from datetime import datetime as dt
from app import app
from flask import Flask
import logging
import pandas as pd
from lib.matching import recommendCourses

####################################################################################
# Add the body
####################################################################################

content = html.Div(id="page-content")

#############################################################################
# Body initial
#############################################################################

body = html.Div(
    children=[
        content,
        html.Footer(html.P("DS4A / Colombia 3.0 - Team 55"), id="footer")
    ], id="body",
    className="ds4a-body",
)

#############################################################################
# Body 1: Students
#############################################################################
TipoConsulta = dbc.FormGroup(
    [
        dbc.Label("Tipo de consulta:", className="query-label"),
        dbc.RadioItems(
            options=[
                {"label": "Individual", "value": 1},
                {"label": "Comparativa", "value": 2},
            ],
            value=1,
            id="Tipo_Consulta",
            className="radiobutton-group",
        ),
    ]
)

# Consulta 1
Consulta_Tipo1_Forms= dbc.Row([
            dbc.Col(
                    [ dbc.Card([dbc.CardHeader("Perfil del estudiante"),
                               dbc.CardBody([student_form.student_form_op])],
                               color="light", outline=True),
                    ], width={"size": 6, "offset": 3},),
        ])

Consulta_Tipo1_Graphs=dbc.Row([
            dbc.Col(
                    [dcc.Graph(id="student-fig-1_op", figure={}, style={'display': 'none'}),
                     dcc.Graph(id="student-fig-2_op", figure={}, style={'display': 'none'}),
                     dcc.Graph(id="student-fig-3_op", figure={}, style={'display': 'none'}),
                     dcc.Graph(id="student-fig-4_op", figure={}, style={'display': 'none'}),
                     dcc.Graph(id="student-fig-5_op", figure={}, style={'display': 'none'}),
                    ],md=12),   
        ])

#Consulta 2
Consulta_Tipo2_Forms= dbc.Row([
            dbc.Col(
                    [dbc.Card([dbc.CardHeader("Perfil del estudiante"),
                               dbc.CardBody([student_form.student_form])],
                               color="light", outline=True),
                    ], md=6),   
            dbc.Col(
                    [dbc.Card([dbc.CardHeader("Perfil del estudiante"),
                                dbc.CardBody([student_form.student_form_op2])]
                               ,color="light", outline=True),
                    ],md=6),  
        ])

Consulta_Tipo2_Graphs=dbc.Row([
                dbc.Col(
                    [   dcc.Graph(id="student-fig-1", figure={}, style={'display': 'none'}),
                        dcc.Graph(id="student-fig-2", figure={}, style={'display': 'none'}),
                        dcc.Graph(id="student-fig-3", figure={}, style={'display': 'none'}),
                        dcc.Graph(id="student-fig-4", figure={}, style={'display': 'none'}),
                        dcc.Graph(id="student-fig-5", figure={}, style={'display': 'none'}),
                    ],md=6),   
                dbc.Col(
                    [   dcc.Graph(id="student-fig-1_op2", figure={}, style={'display': 'none'}),
                        dcc.Graph(id="student-fig-2_op2", figure={}, style={'display': 'none'}),
                        dcc.Graph(id="student-fig-3_op2", figure={}, style={'display': 'none'}),
                        dcc.Graph(id="student-fig-4_op2", figure={}, style={'display': 'none'}),
                        dcc.Graph(id="student-fig-5_op2", figure={}, style={'display': 'none'}),
                    ],md=6),  
            ])

#Cuerpo
body1 = dbc.Container(
    [    
        html.Div(TipoConsulta),
        #TIPO 1 DE CONSULTA
        #Fila para encabezados
        html.Div(Consulta_Tipo1_Forms,id="ConTipo_1_enc",style={'display': 'none'}),       
        #Fila para graficos
        html.Div(Consulta_Tipo1_Graphs,id="ConTipo_1_gra",style={'display': 'none'}), 
        
        #TIPO 2 DE CONSULTA
        #Fila para encabezados
        html.Div(Consulta_Tipo2_Forms,id="ConTipo_2_enc",style={'display': 'none'}),       
        #Fila para graficos
        html.Div(Consulta_Tipo2_Graphs,id="ConTipo_2_gra",style={'display': 'none'}),
        html.Div(
            dbc.Modal(
                [
                    dbc.ModalHeader(html.Div("Error en las fechas de observación",className='modal-Header'),),
                    dbc.ModalBody(
                        [
                            html.Div("La fecha de inicio es mayor a la fecha final."),
                            html.Div("Revisa las fechas de consulta"),
                        ]
                        , className='modal-Body'),
                    dbc.ModalFooter(
                    dbc.Button("Close", id="close_op", className="msg-errorbotton",n_clicks=0)),
                ],
                id="modal_op",
                className="msg_error",
                            centered=True,),
                 ),
        
        html.Div(
            dbc.Modal(
                [
                    dbc.ModalHeader(html.Div("Error en las fechas de observación",className='modal-Header'),),
                    dbc.ModalBody(
                        [
                            html.Div("La fecha de inicio es mayor a la fecha final."),
                            html.Div("Revisa las fechas de consulta"),
                        ]
                        , className='modal-Body'),
                    dbc.ModalFooter(
                    dbc.Button("Close", id="close", className="msg-errorbotton",n_clicks=0)),
                ],
                id="modal",
                className="msg-error",
                            centered=True,),
                 ),
         html.Div(
            dbc.Modal(
                [
                    dbc.ModalHeader(html.Div("Error en las fechas de observación", className='modal-Header'),),
                    dbc.ModalBody(
                        [
                            html.Div("La fecha de inicio es mayor a la fecha final."),
                            html.Div("Revisa las fechas de consulta"),
                        ]
                        , className='modal-Body'),
                    dbc.ModalFooter(
                    dbc.Button("Close", id="close_op2", className="msg-errorbotton",n_clicks=0)),
                ],
                id="modal_op2",
                className="msg-error",
                            centered=True,),
                 ),      
        
    ], id="body1",
    fluid=True
)
####################################
# Call backs para los checkbox
####################################

@app.callback(
    [
        Output('ConTipo_1_enc', 'style'),
        Output('ConTipo_1_gra', 'style'),
        Output('ConTipo_2_enc', 'style'),
        Output('ConTipo_2_gra', 'style'),
    ],
    [Input("Tipo_Consulta", "value")],
)
def on_form_change(Tipo_Consulta_Val):
    if (Tipo_Consulta_Val == 1):
        return [
            {'display': 'block'},
            {'display': 'block'},
            {'display': 'none'},
            {'display': 'none'},
        ]
    else:
        return [

            {'display': 'none'},
            {'display': 'none'},
            {'display': 'block'},
            {'display': 'block'},
        ]

#######################################
# Retorna gráficas para página estudiantes
#######################################

def getStudentPlots(student_start_date, student_end_date, category):
    return [
        plots_methods.getStudentsByAgeAndGender(
            student_start_date, student_end_date, category),
        {'display': 'block'},
        plots_methods.getStudentsByMunicipality(
            student_start_date, student_end_date, category),
        {'display': 'block'},
        plots_methods.getEnrollmentByTrainingCenter(
            student_start_date, student_end_date),
        {'display': 'block'},
        plots_methods.getTopCoursesByAge(
            student_start_date, student_end_date, category),
        {'display': 'block'},
        plots_methods.getTopGroupsByAge(
            student_start_date, student_end_date, category),
        {'display': 'block'},
    ]

####################################
# Call backs - Consulta sencilla
####################################
minDate = '2013-05-06'
maxDate = '2020-08-11'
# Habilitar el botón de busqueda
@app.callback(
    Output('search_student_1_op', 'disabled'),
    [Input('student_start_date_op', 'value'), Input('student_end_date_op', 'value'), Input('category_op', 'value'), ])
def show_buttone_op(start_date_student, end_date_student, category):
    if (start_date_student != None and end_date_student != None and category != None):        
        if (minDate<=start_date_student<=maxDate) and (minDate<=end_date_student<=maxDate):
            return False
        else:
            return True
    else:
        return True

# Habilitar el cuadro de advertencia cuando se digita una fecha errada
@app.callback(
    Output("modal_op", "is_open"),
    [Input("search_student_1_op", "n_clicks"), Input("close_op", "n_clicks")],
    [
        State("modal_op", "is_open"),
        State('student_start_date_op', 'value'),
        State('student_end_date_op', 'value'),
    ],
)
def toggle_modale_op(open_click, close_click, is_open, student_start_date, student_end_date):
    if (open_click > 0 and student_end_date < student_start_date):
        return not is_open

# Muestra las gráficas generadas al oprimir el botón de buscar
@app.callback(
    [
        Output('student-fig-1_op', 'figure'),
        Output('student-fig-1_op', 'style'),
        Output('student-fig-2_op', 'figure'),
        Output('student-fig-2_op', 'style'),
        Output('student-fig-3_op', 'figure'),
        Output('student-fig-3_op', 'style'),
        Output('student-fig-4_op', 'figure'),
        Output('student-fig-4_op', 'style'),
        Output('student-fig-5_op', 'figure'),
        Output('student-fig-5_op', 'style'),

    ],
    [Input('search_student_1_op', 'n_clicks')],
    [
        State('student_start_date_op', 'value'),
        State('student_end_date_op', 'value'),
        State('category_op', 'value'),
    ],
    prevent_initial_callback=True
)
def update_student_plotse_op(n_clicks, student_start_date, student_end_date, category):
    if (n_clicks > 0 and student_start_date != None and student_end_date):

        student_start_date = pd.to_datetime(student_start_date)
        student_end_date = pd.to_datetime(student_end_date)

        if (student_end_date >= student_start_date):
            return getStudentPlots(student_start_date, student_end_date, category)
        else:
            raise dash.exceptions.PreventUpdate
    else:
        raise dash.exceptions.PreventUpdate


####################################
# Call backs - Consulta comparativa
####################################

##########################
# Call backs F1
##########################
def isValidDate(startDate, endDate):
    if (minDate<=startDate<=maxDate) and (minDate<=endDate<=maxDate):
        return True
    else:
        return False

# Habilitar el botón de busqueda
@app.callback(
    Output('search_student_1', 'disabled'),
    [Input('student_start_date', 'value'), Input('student_end_date', 'value'), Input('category', 'value'), ])
def show_button(start_date_student, end_date_student, category):
    if (start_date_student != None and end_date_student != None and category != None):
        if isValidDate(start_date_student, end_date_student):
            return False
        else:
            return True
    else:
        return True

# Habilitar el cuadro de advertencia cuando se digita una fecha errada
@app.callback(
    Output("modal", "is_open"),
    [Input("search_student_1", "n_clicks"), Input("close", "n_clicks")],
    [
        State("modal", "is_open"),
        State('student_start_date', 'value'),
        State('student_end_date', 'value'),
    ],
)
def toggle_modal(open_click, close_click, is_open, student_start_date, student_end_date):
    if (open_click > 0 and student_end_date < student_start_date):
        return not is_open

# Muestra las gráficas generadas al oprimir el botón de buscar
@app.callback(
    [
        Output('student-fig-1', 'figure'),
        Output('student-fig-1', 'style'),
        Output('student-fig-2', 'figure'),
        Output('student-fig-2', 'style'),
        Output('student-fig-3', 'figure'),
        Output('student-fig-3', 'style'),
        Output('student-fig-4', 'figure'),
        Output('student-fig-4', 'style'),
        Output('student-fig-5', 'figure'),
        Output('student-fig-5', 'style'),
    ],
    [Input('search_student_1', 'n_clicks')],
    [
        State('student_start_date', 'value'),
        State('student_end_date', 'value'),
        State('category', 'value'),
    ],
    prevent_initial_callback=True
)
def update_student_plots(n_clicks, student_start_date, student_end_date, category):
    if (n_clicks > 0 and student_start_date != None and student_end_date):

        student_start_date = pd.to_datetime(student_start_date)
        student_end_date = pd.to_datetime(student_end_date)

        if(student_end_date >= student_start_date):
            return getStudentPlots(student_start_date, student_end_date, category)
        else:
            raise dash.exceptions.PreventUpdate
    else:
        raise dash.exceptions.PreventUpdate


##########################
# Call backs F2
##########################

# Habilitar el botón de busqueda
@app.callback(
    Output('search_student_1_op2', 'disabled'),
    [Input('student_start_date_op2', 'value'), Input('student_end_date_op2', 'value'), Input('category_op2', 'value'), ])
def show_buttone_op2(start_date_student_op2, end_date_student_op2, category_op2):
    if (start_date_student_op2 != None and end_date_student_op2 != None and category_op2 != None):
        if isValidDate(start_date_student_op2, end_date_student_op2):        
            return False
        else:
            return True
    else:
        return True

# Habilitar el cuadro de advertencia cuando se digita una fecha errada
@app.callback(
    Output("modal_op2", "is_open"),
    [Input("search_student_1_op2", "n_clicks"),
     Input("close_op2", "n_clicks")],
    [
        State("modal_op2", "is_open"),
        State('student_start_date_op2', 'value'),
        State('student_end_date_op2', 'value'),
    ],
)
def toggle_modal_op2(open_click, close_click, is_open, student_start_date, student_end_date):
    if (open_click > 0 and student_end_date < student_start_date):
        return not is_open

# Muestra las gráficas generadas al oprimir el botón de buscar
@app.callback(
    [
        Output('student-fig-1_op2', 'figure'),
        Output('student-fig-1_op2', 'style'),
        Output('student-fig-2_op2', 'figure'),
        Output('student-fig-2_op2', 'style'),
        Output('student-fig-3_op2', 'figure'),
        Output('student-fig-3_op2', 'style'),
        Output('student-fig-4_op2', 'figure'),
        Output('student-fig-4_op2', 'style'),
        Output('student-fig-5_op2', 'figure'),
        Output('student-fig-5_op2', 'style'),

    ],
    [Input('search_student_1_op2', 'n_clicks')],
    [
        State('student_start_date_op2', 'value'),
        State('student_end_date_op2', 'value'),
        State('category_op2', 'value'),
    ],
    prevent_initial_callback=True
)
def update_student_plots_op2(n_clicks, student_start_date, student_end_date, category):
    if (n_clicks > 0 and student_start_date != None and student_end_date):

        student_start_date = pd.to_datetime(student_start_date)
        student_end_date = pd.to_datetime(student_end_date)

        if(student_end_date >= student_start_date):
            return getStudentPlots(student_start_date, student_end_date, category)
        else:
            raise dash.exceptions.PreventUpdate
    else:
        raise dash.exceptions.PreventUpdate

#############################################################################
# Body 2: Courses
#############################################################################

#######################################
# Retorna gráficas para página cursos
#######################################
def getCoursesPlots(select_search, course_start_date, course_end_date, search_id_items):
    if select_search == 'curso':
        return [
            plots_methods.getCategoryByCourses(
                course_start_date, course_end_date, search_id_items),
            {'display': 'block'},
            plots_methods.getAgeByCourses(
                course_start_date, course_end_date, search_id_items),
            {'display': 'block'},
            plots_methods.getRevenueByCourses(
                course_start_date, course_end_date, search_id_items),
            {'display': 'block'},
        ]
    if select_search == 'programa':
        return [
            plots_methods.getCategoryByProgram(
                course_start_date, course_end_date, search_id_items),
            {'display': 'block'},
            plots_methods.getAgeByProgram(
                course_start_date, course_end_date, search_id_items),
            {'display': 'block'},
            plots_methods.getRevenueByProgram(
                course_start_date, course_end_date, search_id_items),
            {'display': 'block'},
        ]

    if select_search == 'area':
        return [
            plots_methods.getCategoryByArea(
                course_start_date, course_end_date, search_id_items),
            {'display': 'block'},
            plots_methods.getAgeByArea(
                course_start_date, course_end_date, search_id_items),
            {'display': 'block'},
            plots_methods.getRevenueByArea(
                course_start_date, course_end_date, search_id_items),
            {'display': 'block'},
        ]

TipoConsulta_C = dbc.FormGroup(
    [
        dbc.Label("Tipo de consulta:", className="query-label"),
        dbc.RadioItems(
            options=[
                {"label": "Individual", "value": 1},
                {"label": "Comparativa", "value": 2},
            ],
            value=1,
            id="Tipo_Consulta_C",
            className="radiobutton-group",
        ),
    ]
)
# Consulta 1
Consulta_Tipo1_FormsC = dbc.Row([
    dbc.Col(
        [dbc.Card([dbc.CardHeader("Criterios de búsqueda"),
                   dbc.CardBody([course_form.course_form])],
                  color="light", outline=True),
         ], width={"size": 6, "offset": 3},),
])

Consulta_Tipo1_GraphsC = dbc.Row([
    dbc.Col(
        [dcc.Graph(id="course-fig-1", figure={}, style={'display': 'none'}),
         dcc.Graph(id="course-fig-2", figure={},
                   style={'display': 'none'}),
         dcc.Graph(id="course-fig-3", figure={},
                   style={'display': 'none'}),
         ], md=12),
])

#Consulta 2
Consulta_Tipo2_FormsC= dbc.Row([
            dbc.Col(
                    [dbc.Card([dbc.CardHeader("Perfil del estudiante"),
                               dbc.CardBody([course_form.course_form_op1])],
                               color="light", outline=True),
                    ], md=6),   
            dbc.Col(
                    [dbc.Card([dbc.CardHeader("Perfil del estudiante"),
                                dbc.CardBody([course_form.course_form_op2])]
                               ,color="light", outline=True),
                    ],md=6),  
        ])

Consulta_Tipo2_GraphsC=dbc.Row([
                dbc.Col(
                    [   dcc.Graph(id="course-fig-1_op1", figure={}, style={'display': 'none'}),
                        dcc.Graph(id="course-fig-2_op1", figure={}, style={'display': 'none'}),
                        dcc.Graph(id="course-fig-3_op1", figure={}, style={'display': 'none'}), 
                    ],md=6),   
                dbc.Col(
                    [   dcc.Graph(id="course-fig-1_op2", figure={}, style={'display': 'none'}),
                        dcc.Graph(id="course-fig-2_op2", figure={}, style={'display': 'none'}),
                        dcc.Graph(id="course-fig-3_op2", figure={}, style={'display': 'none'}),
                    ],md=6),  
            ])

course_list = []

# Cuerpo
body2 = dbc.Container(
    [
        html.Div(TipoConsulta_C),
        # TIPO 1 DE CONSULTA
        # Fila para encabezados
        html.Div(Consulta_Tipo1_FormsC, id="ConTipo_1F_courses",
                 style={'display': 'none'}),
        # Fila para graficos
        html.Div(Consulta_Tipo1_GraphsC, id="ConTipo_1G_courses",
                 style={'display': 'none'}),

        # TIPO 2 DE CONSULTA
        # Fila para encabezados
        html.Div(Consulta_Tipo2_FormsC, id="ConTipo_2F_courses",
                 style={'display': 'none'}),
        # Fila para graficos
        html.Div(Consulta_Tipo2_GraphsC, id="ConTipo_2G_courses",
                 style={'display': 'none'}),

    ],
    id="body2",
    fluid=True
)
####################################
# Call backs para los checkbox
####################################

@app.callback(
    [
        Output('ConTipo_1F_courses', 'style'),
        Output('ConTipo_1G_courses', 'style'),
        Output('ConTipo_2F_courses', 'style'),
        Output('ConTipo_2G_courses', 'style'),
    ],
    [Input("Tipo_Consulta_C", "value")],
)
def on_form_change_C(Tipo_Consulta_Val):
    if (Tipo_Consulta_Val == 1):
        return [
            {'display': 'block'},
            {'display': 'block'},
            {'display': 'none'},
            {'display': 'none'},
        ]
    else:
        return [

            {'display': 'none'},
            {'display': 'none'},
            {'display': 'block'},
            {'display': 'block'},
        ]

####################################
# Call backs - Consulta SENCILLA
####################################

# Habilitar el botón de busqueda
@app.callback(
    Output('search_courses', 'disabled'),
    [Input('course_start_date', 'value'), Input('course_end_date', 'value'), Input('select_search', 'value'), ])
def show_button_C(course_start_date, course_end_date, select_search):
    if (course_start_date != None and course_end_date != None and select_search != None):
        if isValidDate(course_start_date, course_end_date):        
            return False
        else:
            return True
    else:
        return True

@app.callback(
    [
        Output('course-fig-1', 'figure'),
        Output('course-fig-1', 'style'),
        Output('course-fig-2', 'figure'),
        Output('course-fig-2', 'style'),
        Output('course-fig-3', 'figure'),
        Output('course-fig-3', 'style'),
    ],
    [Input('search_courses', 'n_clicks')],
    [
        State('course_start_date', 'value'),
        State('course_end_date', 'value'),
        State('select_search', 'value'),
        State('search_id_items', 'value'),
    ],
    prevent_initial_callback=True
)
def update_courses_plot(n_clicks, course_start_date, course_end_date, select_search, search_id_items):
    if(n_clicks > 0 and course_start_date != None and course_end_date != None and search_id_items != None):        
        if(course_end_date >= course_start_date):
            if isValidDate(course_start_date, course_end_date):
                return getCoursesPlots(select_search, course_start_date, course_end_date, search_id_items)
        else:
            raise dash.exceptions.PreventUpdate
    else:
        raise dash.exceptions.PreventUpdate

####################################
# Call backs - Consulta comparativa
####################################

# Formato 1:

# Habilitar el botón de busqueda
@app.callback(
    Output('search_courses_op1', 'disabled'),
    [Input('course_start_date_op1', 'value'), Input('course_end_date_op1', 'value'), Input('select_search_op1', 'value'), ])
def show_button_C_op1(course_start_date, course_end_date, select_search):
    if (course_start_date != None and course_end_date != None and select_search != None):        
        if isValidDate(course_start_date, course_end_date):        
            return False
        else:
            return True        
    else:
        return True

@app.callback(
    [
        Output('course-fig-1_op1', 'figure'),
        Output('course-fig-1_op1', 'style'),
        Output('course-fig-2_op1', 'figure'),
        Output('course-fig-2_op1', 'style'),
        Output('course-fig-3_op1', 'figure'),
        Output('course-fig-3_op1', 'style'),
    ],
    [Input('search_courses_op1', 'n_clicks')],
    [
        State('course_start_date_op1', 'value'),
        State('course_end_date_op1', 'value'),
        State('select_search_op1', 'value'),
        State('search_id_items_op1', 'value'),
    ],
    prevent_initial_callback=True
)
def update_courses_plot_op1(n_clicks, course_start_date, course_end_date, select_search, search_id_items):
    if(n_clicks > 0 and course_start_date != None and course_end_date != None and search_id_items != None):
        if(course_end_date >= course_start_date):            
                return getCoursesPlots(select_search, course_start_date, course_end_date, search_id_items)            
        else:
            raise dash.exceptions.PreventUpdate
    else:
        raise dash.exceptions.PreventUpdate

# Formato 2:
# Habilitar el botón de busqueda
@app.callback(
    Output('search_courses_op2', 'disabled'),
    [
        Input('course_start_date_op2', 'value'), 
        Input('course_end_date_op2', 'value'), 
        Input('select_search_op2', 'value'), 
    ]
)
def show_button_C_op2(course_start_date, course_end_date, select_search):
    if (course_start_date != None and course_end_date != None and select_search != None):
        global minDate
        global maxDate        
        if (minDate<=course_start_date<=maxDate) and (minDate<=course_end_date<=maxDate):
            return False
        else:
            return True    
    else:
        return True

@app.callback(
    [
        Output('course-fig-1_op2', 'figure'),
        Output('course-fig-1_op2', 'style'),
        Output('course-fig-2_op2', 'figure'),
        Output('course-fig-2_op2', 'style'),
        Output('course-fig-3_op2', 'figure'),
        Output('course-fig-3_op2', 'style'),
    ],
    [Input('search_courses_op2', 'n_clicks')],
    [
        State('course_start_date_op2', 'value'),
        State('course_end_date_op2', 'value'),
        State('select_search_op2', 'value'),
        State('search_id_items_op2', 'value'),
    ],
    prevent_initial_callback=True
)
def update_courses_plot_op2(n_clicks, course_start_date, course_end_date, select_search, search_id_items):
    if(n_clicks > 0 and course_start_date != None and course_end_date != None and search_id_items != None):
        if(course_end_date >= course_start_date):
            if (minDate<=course_start_date<=maxDate) and (minDate<=course_end_date<=maxDate):
                return getCoursesPlots(select_search, course_start_date, course_end_date, search_id_items)
        else:
            raise dash.exceptions.PreventUpdate
    else:
        raise dash.exceptions.PreventUpdate


#############################################################################
# Body 3: Recomendation
#############################################################################
TipoConsulta3 = dbc.FormGroup(
    [
        dbc.Label("Seleccione la forma en la que desea hacer la recomendación "),
        dbc.RadioItems(
            options=[
                {"label": "Individual", "value": 1},
                {"label": "Grupo de estudiantes", "value": 2},
            ],
            value=1,
            id="TipoConsulta3",
        ),
    ]
)

#Recomendations
body3 = dbc.Container(
    [
        # html.Div(TipoConsulta3),
        html.Br(),
        html.Div(recommendation_form.Form3, id="Recomendation"),
        html.Br(),
        dbc.Spinner([
            html.Div(recommendation_cards.row_1, id="Reco_Fila1", ),
            html.Br(),
            html.Div(recommendation_cards.row_2, id="Reco_Fila2", ),            
            html.Div(id='my-div3', style={'display': 'none'}),
        ],size="lg",  color="success", type="border",),
        html.Div(id='intermediate_div_recommendation', style={'display': 'none'}),  
        html.Div(
            dbc.Modal(
                [
                    dbc.ModalHeader(html.Div("Error en la edad de observación",className='modal-Header'),),
                    dbc.ModalBody(
                        [
                            html.Div("La edad debe estar entre 0 y 100 años."),
                        ]
                        , className='modal-Body'),
                    dbc.ModalFooter(
                    dbc.Button("Close", id="close_Fin", className="msg-errorbotton",n_clicks=0)),
                ],
                id="modal_Fin",
                className="msg-error",
                            centered=True,),
                 ),
    ], id="body3",
    fluid=True
)

####################################
# Call backs para los checkbox
####################################

# Habilitar el cuadro de advertencia cuando se digita una fecha errada
@app.callback(
    Output("modal_Fin", "is_open"),
    [Input("recommend_courses", "n_clicks"), Input("close_Fin", "n_clicks")],
    [
        State("modal_Fin", "is_open"),
        State('age', 'value'),
    ],
)
def toggle_modale_fin(open_click, close_click, is_open, age,):
    if (open_click > 0 and (age<0 or age>100)):
        return not is_open
    
    
    

# Habilitar el botón de busqueda
@app.callback(
    Output('recommend_courses', 'disabled'),
    [
        Input('gender', 'value'),
        Input('age', 'value'),
        Input('town', 'value'),
        Input('educational_stage', 'value'),
        Input('category_recommendation', 'value'),
        Input('course_modality', 'value'),
    ]
)
def enable_recommendation_button(gender, age, town, category_recommendation, educational_stage, course_modality):
    if (None in [gender, age, town, category_recommendation, educational_stage, course_modality]):
        return True
    else:
        return False

# Muestra tarjetas de cursos recomendados al oprimir el botón de buscar
@app.callback(
    [
        Output('intermediate_div_recommendation', 'clear_data'),
        Output('program-card1', 'children'),
        Output('program-card2', 'children'),
        Output('program-card3', 'children'),
        Output('program-card4', 'children'),
        Output('program-card5', 'children'),
        Output('program-card6', 'children'),
        Output('course-card1', 'children'),
        Output('course-card2', 'children'),
        Output('course-card3', 'children'),
        Output('course-card4', 'children'),
        Output('course-card5', 'children'),
        Output('course-card6', 'children'),
       
        Output('accuracy-card1', 'children'),
        Output('accuracy-card2', 'children'),
        Output('accuracy-card3', 'children'),
        Output('accuracy-card4', 'children'),
        Output('accuracy-card5', 'children'),
        Output('accuracy-card6', 'children'),
    ],
    [
        Input('intermediate_div_recommendation', 'children'),
        Input('recommend_courses', 'n_clicks'),
    ],
    prevent_initial_callback=True
)
def update_tar(recommendation, n_clicks):
    if recommendation !=None:
        recommendation_df = pd.read_json(recommendation, orient='split')
        
        print(recommendation_df[['best_match_score', 'dscurso']])
        programs = list(recommendation_df['dsprograma'])         
        courses = list(recommendation_df['dscurso'])
        recommendation_df['best_match_score'] = recommendation_df['best_match_score'].apply(lambda x: 'Porcentaje de afinidad: '+ str(x) + "%")
        score = list(recommendation_df['best_match_score'])
        recommendations_amount = len(recommendation_df)

        if recommendations_amount < 6:
            for i in range(recommendations_amount, 6):
                programs.append('')
                courses.append('')
                score.append('')
               
        
        return[
            True,
            programs[0],programs[1],programs[2],
            programs[3],programs[4],programs[5],
            courses[0],courses[1], courses[2],
            courses[3],courses[4], courses[5],
            
            score[0],score[1], score[2],
            score[3],score[4], score[5],
        ]
    else:
        raise dash.exceptions.PreventUpdate

@app.callback(
    [
        Output('intermediate_div_recommendation', 'children'),
        Output('card1', 'style'),
        Output('card2', 'style'),
        Output('card3', 'style'),
        Output('card4', 'style'),
        Output('card5', 'style'),
        Output('card6', 'style'),
        Output('my-div3', 'children'),
    ],
    [Input('recommend_courses', 'n_clicks')],
    [
        State('gender', 'value'),
        State('age', 'value'),
        State('town', 'value'),
        State('educational_stage', 'value'),
        State('category_recommendation', 'value'),
        State('course_modality', 'value'),
    ],
    prevent_initial_callback=True
)
def update_output_div3(n_clicks, gender, age, town, educational_stage, category_recommendation, course_modality):
    if (n_clicks == 0):
        raise dash.exceptions.PreventUpdate
    elif (n_clicks >= 0 and None in [gender, gender, age, town, category_recommendation, educational_stage, course_modality]):
        raise dash.exceptions.PreventUpdate
    else:
        if(age<0) or (age > 100):
            raise dash.exceptions.PreventUpdate
        else:
            recommendation_df = recommendCourses(gender, age, town, category_recommendation, educational_stage, course_modality)

            if len(recommendation_df)>0:
                visibility = []

                for i in range (0,len(recommendation_df)):
                    visibility.append({'display': 'block'})

                recommendations_amount = len(recommendation_df)

                if recommendations_amount < 6:
                    for i in range(recommendations_amount, 6):                
                        visibility.append({'display': 'none'})
        
                return [
                    recommendation_df.to_json(orient='split'),            
                    visibility[0],visibility[1], visibility[2],
                    visibility[3],visibility[4], visibility[5],
                    u'''
                    Search Button has been pressed {} times,
                    Gender is {}, age is {},
                    educational_stage is {},
                    town is {},
                    category is {},
                    course modality is {},
                    '''.format(n_clicks, gender, age, educational_stage, town, category_recommendation, course_modality),
                ]