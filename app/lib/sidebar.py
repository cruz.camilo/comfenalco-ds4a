# Basics Requirements
import pathlib
import dash
from dash.dependencies import Input, Output, State, ClientsideFunction
import dash_core_components as dcc
import dash_html_components as html


# Dash Bootstrap Components
import dash_bootstrap_components as dbc

# Recall app
from app import app


####################################################################################
# Add the DS4A_Img
####################################################################################


DS4A_Img = html.Div(
    children=[        
        html.A(
            html.Img(src=app.get_asset_url("LOGO_comfenalco.svg")),
            href="https://www.comfenalcoantioquia.com.co/personas",
            target="_blank"
        )
    ]
)



Pages=html.Div(
     children=[dbc.Nav(
                [
                    dbc.NavLink("Estudiantes", href="/page-1", id="page-1-link"),
                    dbc.NavLink("Cursos", href="/page-2", id="page-2-link"),
                    dbc.NavLink("Recomendación de Cursos",href="/page-3", id="page-3-link"),
            
                ],
                vertical=True,
                pills=True,
                id="ds4a-pages", 
                className="nav nav-pills",)
           ])



#############################################################################
# Sidebar Layout
#############################################################################
sidebar = html.Div(
    [
        DS4A_Img,
        html.Hr(),  # Add an horizontal line
        ####################################################
        # Place the rest of Layout here
        ###################################################
        Pages,
        html.Hr(),  # Add an horizontal line
        html.Footer(  
            children=[ 
                html.A(
                    html.Img(src= app.get_asset_url("ds4a-img.svg")),
                    href="https://www.correlation-one.com/ds4a-latam",
                    target="_blank"
                ),
            ],                
            id="footerR",
        ),                            
    ],
    className="ds4a-sidebar",
)
