
from lists import *
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import lists
import dash
from dash.dependencies import Input, Output, State, ClientsideFunction
from app import app
from lib import body

#Listas search_options:
cursos_list = []
cursos_id = []
programas_list = []
programas_id = []
areas_list = []
areas_id = []

cursos_list_op1 = []
cursos_id_op1  = []
programas_list_op1  = []
programas_id_op1  = []
areas_list_op1  = []
areas_id_op1  = []

cursos_list_op2  = []
cursos_id_op2  = []
programas_list_op2  = []
programas_id_op2  = []
areas_list_op2  = []
areas_id_op2  = []

#############################################################################
# Componentes
#############################################################################

#Componentes para el formato cero
course_start_date = dbc.FormGroup(
    [
        dbc.Label('Desde:', width=4, className='label-horizontal'),
        dbc.Col(
            dbc.Input(type="date", className="time_input", id='course_start_date', min="2013-05-06", max="2020-08-11"),
            width=8,
        ),
    ],
    row=True,
)

course_end_date = dbc.FormGroup(
    [
        dbc.Label('Hasta:', width=4, className='label-horizontal'),
        dbc.Col(
            dbc.Input(type="date", className="time_input",  id='course_end_date', min="2013-05-06", max="2020-08-11"),
            width=8,
        ),
    ],
    row=True,
)

select_search = dbc.FormGroup(
    [
        dbc.Label('Buscar por:', width=4, className='label-horizontal'),
        dbc.Col(
            dcc.Dropdown(
                options=[
                    {'label': 'Area', 'value': 'area'},
                    {'label': 'Programa', 'value': 'programa'},
                    {'label': 'Curso', 'value': 'curso'} 
                ],
                id='select_search',
                placeholder='Area, programa, curso',
                className='drop',
            ),
            width=8,
        ),
    ],
   row=True,
)

search_options = dbc.FormGroup(
    [
        dbc.Label(width=4, className='label-horizontal', style={'display': 'none'}, id='search_options_label'),        
        dbc.Col(
            dcc.Dropdown(
                id='search_id_items',
                multi=True,            
                className='drop',                
            ),
            id='search_col',
            style={'display': 'none'},
            width=8,
        )
    ],
    row=True,
)

submit = dbc.FormGroup(
    [
        dbc.Button(
            "Buscar",
            className="buttonS1",
            id='search_courses',
            n_clicks=0,
            block=True,
            disabled=True
        )
    ]
)

#Componentes para el formato 1
course_start_date_op1 = dbc.FormGroup(
    [
        dbc.Label('Desde:', width=4, className='label-horizontal'),
        dbc.Col(
            dbc.Input(type="date", className="time_input", id='course_start_date_op1', min="2013-05-06", max="2020-08-11"),
            width=8,
        ),
    ],
    row=True,
)

course_end_date_op1 = dbc.FormGroup(
    [
        dbc.Label('Hasta:', width=4, className='label-horizontal'),
        dbc.Col(
            dbc.Input(type="date", className="time_input",  id='course_end_date_op1', min="2013-05-06", max="2020-08-11"),
            width=8,
        ),
    ],
    row=True,
)

select_search_op1 = dbc.FormGroup(
    [
        dbc.Label('Buscar por:', width=4, className='label-horizontal'),
        dbc.Col(
            dcc.Dropdown(
                options=[
                    {'label': 'Area', 'value': 'area'},
                    {'label': 'Programa', 'value': 'programa'},
                    {'label': 'Curso', 'value': 'curso'} 
                ],
                id='select_search_op1',
                placeholder='Area, programa, curso',
                className='drop',
            ),
            width=8,
        ),
    ],
   row=True,
)

search_options_op1 = dbc.FormGroup(
    [
        dbc.Label(width=4, className='label-horizontal', style={'display': 'none'}, id='search_options_label_op1'),        
        dbc.Col(
            dcc.Dropdown(
                id='search_id_items_op1',
                multi=True,            
                className='drop',                
            ),
            id='search_col_op1',
            style={'display': 'none'},
            width=8,
        )
    ],
    row=True,
)

submit_op1 = dbc.FormGroup(
    [
        dbc.Button(
            "Buscar",
            className="buttonS1",
            id='search_courses_op1',
            n_clicks=0,
            block=True,
            disabled=True
        )
    ]
)

#Componentes para el formato 2
course_start_date_op2 = dbc.FormGroup(
    [
        dbc.Label('Desde:', width=4, className='label-horizontal'),
        dbc.Col(
            dbc.Input(type="date", className="time_input", id='course_start_date_op2', min="2013-05-06", max="2020-08-11"),
            width=8,
        ),
    ],
    row=True,
)

course_end_date_op2 = dbc.FormGroup(
    [
        dbc.Label('Hasta:', width=4, className='label-horizontal'),
        dbc.Col(
            dbc.Input(type="date", className="time_input",  id='course_end_date_op2', min="2013-05-06", max="2020-08-11"),
            width=8,
        ),
    ],
    row=True,
)

select_search_op2 = dbc.FormGroup(
    [
        dbc.Label('Buscar por:', width=4, className='label-horizontal'),
        dbc.Col(
            dcc.Dropdown(
                options=[
                    {'label': 'Area', 'value': 'area'},
                    {'label': 'Programa', 'value': 'programa'},
                    {'label': 'Curso', 'value': 'curso'} 
                ],
                id='select_search_op2',
                placeholder='Area, programa, curso',
                className='drop',
            ),
            width=8,
        ),
    ],
   row=True,
)

search_options_op2 = dbc.FormGroup(
    [
        dbc.Label(width=4, className='label-horizontal', style={'display': 'none'}, id='search_options_label_op2'),        
        dbc.Col(
            dcc.Dropdown(
                id='search_id_items_op2',
                multi=True,            
                className='drop',                
            ),
            id='search_col_op2',
            style={'display': 'none'},
            width=8,
        )
    ],
    row=True,
)

submit_op2 = dbc.FormGroup(
    [
        dbc.Button(
            "Buscar",
            className="buttonS1",
            id='search_courses_op2',
            n_clicks=0,
            block=True,
            disabled=True
        )
    ]
)
###############################################################
# fORMATOS
#################################################################
#Formato 0
course_form = dbc.Form(
    [        
        course_start_date,
        course_end_date,
        select_search,        
        search_options,
        submit
    ],
    id='left_form'
)
#Formato 1
course_form_op1 = dbc.Form(
    [        
        course_start_date_op1,
        course_end_date_op1,
        select_search_op1,        
        search_options_op1,
        submit_op1
    ],
    id='left_form_op1'
)
#Formato 2
course_form_op2 = dbc.Form(
    [        
        course_start_date_op2,
        course_end_date_op2,
        select_search_op2,        
        search_options_op2,
        submit_op2
    ],
    id='left_form_op2'
)



###############################################################
# Callbacks: lISTA OPCIÓN 0
#################################################################
@app.callback(  
    [
        Output('search_options_label', 'children'),
        Output('search_id_items', 'placeholder'),
        Output('search_col', 'style'),
        Output('search_options_label', 'style'),
        Output('search_id_items', 'options'),      
    ],
    [
        Input('select_search', 'value'),
        Input('course_start_date', 'value'),
        Input('course_end_date', 'value'),
    ]
)
def update_dropdown_list(select_search, course_start_date, course_end_date):
    if (course_start_date != None and course_end_date != None and select_search != None 
    and body.isValidDate(course_start_date, course_end_date)):
        course_start_date = pd.to_datetime(course_start_date)
        course_end_date = pd.to_datetime(course_end_date)

        if select_search == 'curso':
            df_courses = db.getCoursesList(course_start_date, course_end_date)

            df_courses['dscurso_2'] = df_courses['dscurso_2'].apply(lambda x: (x.lower()).title())
            
            global cursos_list
            global cursos_id              
            cursos_list = list(df_courses['dscurso_2'])
            cursos_id = list(df_courses['nmsec_curso'])

            return [          
                "Cursos:",
                'Selecciona los cursos',
                {'display':'block'},
                {'display':'block'},
                [{'label': cursos_list[i], 'value': cursos_id[i]} for i in range (0, len(cursos_list))], 
                ]

        elif select_search == 'programa':

            df_programas = db.getProgramsList(course_start_date, course_end_date)

            df_programas['dsprograma'] = df_programas['dsprograma'].apply(lambda x: (x.lower()).title())

            global programas_list
            global programas_id
            programas_list = list(df_programas['dsprograma'])
            programas_id = list(df_programas['nmsec_programa'])

            return [
                "Programas:",
                'Selecciona los programas',
                {'display':'block'},
                {'display':'block'},
                [{'label': programas_list[i], 'value': programas_id[i]} for i in range (0, len(programas_list))],
                ]

        else:
            df_areas = db.getAreasList(course_start_date, course_end_date)

            df_areas['dsarea'] = df_areas['dsarea'].apply(lambda x: (x.lower()).title())

            global areas_list
            global areas_id
            areas_list = list(df_areas['dsarea'])
            areas_id = list(df_areas['nmsec_area'])
        
            return [
                "Areas:",
                'Selecciona las areas',
                {'display':'block'},
                {'display':'block'},
                [{'label': areas_list[i], 'value': areas_id[i]} for i in range (0, len(areas_list))],
                ]
    else:
        raise dash.exceptions.PreventUpdate
        
        
        
        
###############################################################
# Callbacks: lISTA OPCIÓN 1
#################################################################
@app.callback(  
    [
        Output('search_options_label_op1', 'children'),
        Output('search_id_items_op1', 'placeholder'),
        Output('search_col_op1', 'style'),
        Output('search_options_label_op1', 'style'),
        Output('search_id_items_op1', 'options'),      
    ],
    [
        Input('select_search_op1', 'value'),
        Input('course_start_date_op1', 'value'),
        Input('course_end_date_op1', 'value'),
    ]
)
def update_dropdown_list_op1(select_search, course_start_date, course_end_date):
    if (course_start_date != None and course_end_date != None and select_search != None 
    and body.isValidDate(course_start_date, course_end_date)):
        course_start_date = pd.to_datetime(course_start_date)
        course_end_date = pd.to_datetime(course_end_date)

        if select_search == 'curso':
            df_courses = db.getCoursesList(course_start_date, course_end_date)

            df_courses['dscurso_2'] = df_courses['dscurso_2'].apply(lambda x: (x.lower()).title())
            
            global cursos_list_op1
            global cursos_id_op1             
            cursos_list_op1 = list(df_courses['dscurso_2'])
            cursos_id_op1= list(df_courses['nmsec_curso'])

            return [          
                "Cursos:",
                'Selecciona los cursos',
                {'display':'block'},
                {'display':'block'},
                [{'label': cursos_list_op1[i], 'value': cursos_id_op1[i]} for i in range (0, len(cursos_list_op1))], 
                ]

        elif select_search == 'programa':

            df_programas = db.getProgramsList(course_start_date, course_end_date)

            df_programas['dsprograma'] = df_programas['dsprograma'].apply(lambda x: (x.lower()).title())

            global programas_list_op1
            global programas_id_op1
            programas_list_op1 = list(df_programas['dsprograma'])
            programas_id_op1 = list(df_programas['nmsec_programa'])

            return [
                "Programas:",
                'Selecciona los programas',
                {'display':'block'},
                {'display':'block'},
                [{'label': programas_list_op1[i], 'value': programas_id_op1[i]} for i in range (0, len(programas_list_op1))],
                ]

        else:
            df_areas = db.getAreasList(course_start_date, course_end_date)

            df_areas['dsarea'] = df_areas['dsarea'].apply(lambda x: (x.lower()).title())

            global areas_list_op1
            global areas_id_op1
            areas_list_op1 = list(df_areas['dsarea'])
            areas_id_op1 = list(df_areas['nmsec_area'])
        
            return [
                "Areas:",
                'Selecciona las areas',
                {'display':'block'},
                {'display':'block'},
                [{'label': areas_list_op1[i], 'value': areas_id_op1[i]} for i in range (0, len(areas_list_op1))],
                ]
    else:
        raise dash.exceptions.PreventUpdate
        
###############################################################
# Callbacks: lISTA OPCIÓN 2
#################################################################
@app.callback(  
    [
        Output('search_options_label_op2', 'children'),
        Output('search_id_items_op2', 'placeholder'),
        Output('search_col_op2', 'style'),
        Output('search_options_label_op2', 'style'),
        Output('search_id_items_op2', 'options'),      
    ],
    [
        Input('select_search_op2', 'value'),
        Input('course_start_date_op2', 'value'),
        Input('course_end_date_op2', 'value'),
    ]
)
def update_dropdown_list_op2(select_search, course_start_date, course_end_date):
    if (course_start_date != None and course_end_date != None and select_search != None 
    and body.isValidDate(course_start_date, course_end_date)):
        course_start_date = pd.to_datetime(course_start_date)
        course_end_date = pd.to_datetime(course_end_date)

        if select_search == 'curso':
            df_courses = db.getCoursesList(course_start_date, course_end_date)

            df_courses['dscurso_2'] = df_courses['dscurso_2'].apply(lambda x: (x.lower()).title())
            
            global cursos_list_op2
            global cursos_id_op2             
            cursos_list_op2 = list(df_courses['dscurso_2'])
            cursos_id_op2= list(df_courses['nmsec_curso'])

            return [          
                "Cursos:",
                'Selecciona los cursos',
                {'display':'block'},
                {'display':'block'},
                [{'label': cursos_list_op2[i], 'value': cursos_id_op2[i]} for i in range (0, len(cursos_list_op2))], 
                ]

        elif select_search == 'programa':

            df_programas = db.getProgramsList(course_start_date, course_end_date)

            df_programas['dsprograma'] = df_programas['dsprograma'].apply(lambda x: (x.lower()).title())

            global programas_list_op2
            global programas_id_op2
            programas_list_op2 = list(df_programas['dsprograma'])
            programas_id_op2 = list(df_programas['nmsec_programa'])

            return [
                "Programas:",
                'Selecciona los programas',
                {'display':'block'},
                {'display':'block'},
                [{'label': programas_list_op2[i], 'value': programas_id_op2[i]} for i in range (0, len(programas_list_op2))],
                ]

        else:
            df_areas = db.getAreasList(course_start_date, course_end_date)

            df_areas['dsarea'] = df_areas['dsarea'].apply(lambda x: (x.lower()).title())

            global areas_list_op2
            global areas_id_op2
            areas_list_op2 = list(df_areas['dsarea'])
            areas_id_op2 = list(df_areas['nmsec_area'])
        
            return [
                "Areas:",
                'Selecciona las areas',
                {'display':'block'},
                {'display':'block'},
                [{'label': areas_list_op2[i], 'value': areas_id_op2[i]} for i in range (0, len(areas_list_op2))],
                ]
    else:
        raise dash.exceptions.PreventUpdate
        