import pandas as pd
import re
import io
import numpy as np
import plotly.express as px
import plotly.graph_objects as go
import time
from lib import db

from datetime import datetime

# from app import app
# from index import app

def getInscripcionAlumnosSubDf(startDate, endDate, category): 
    
    if(category==["T"]):
        CatEval=['A','B','C','D']
    else:
        CatEval=category
       
    subdf_inscripcion = db.df_inscripcion[(db.df_inscripcion['fepago'] >= startDate) & (
        db.df_inscripcion['fepago'] < endDate)]    

    df_alumnos_inscripciones = pd.merge(subdf_inscripcion[[
                                        "nmsec_dni_alumno", 'fepago', 'nmsec_grupo', 'valor']], db.df_alumnos, left_on='nmsec_dni_alumno', right_on='nmsec_dni')
    df_alumnos_inscripciones = pd.merge(
        df_alumnos_inscripciones, db.df_alumnos_escolaridad, left_on='nmsec_dni_alumno', right_on='nmsec_dni')
    df_alumnos_inscripciones['categoria'] = df_alumnos_inscripciones['categoria'].apply(lambda x: 'D' if x is None else x)
       
    #Cambio esta fila porque debe tener el valor de la variable temporal y no de category
    if CatEval:        
        df_alumnos_inscripciones = df_alumnos_inscripciones[df_alumnos_inscripciones['categoria'].isin(
            CatEval)]

    df_alumnos_inscripciones['edad1'] = (pd.to_datetime(
        df_alumnos_inscripciones['fepago']) - pd.to_datetime(df_alumnos_inscripciones['fenacimiento'])).astype('<m8[Y]')

    mean_age = round(
        df_alumnos_inscripciones[df_alumnos_inscripciones['edad1'] < 100]['edad1'].mean(), 0)
    df_alumnos_inscripciones['edad'] = np.where(df_alumnos_inscripciones['edad1'] >= 100, mean_age, df_alumnos_inscripciones['edad1'])

    df_alumnos_inscripciones = addGrupoEdadColumn(df_alumnos_inscripciones)
    df_alumnos_inscripciones = df_alumnos_inscripciones.drop(columns=['edad1'], axis=1)

    return df_alumnos_inscripciones

def addGrupoEdadColumn(dataframe):
    dataframe['grupo_edad'] = np.select([
        (dataframe['edad'].le(5)),
        (dataframe['edad'].gt(5) & dataframe['edad'].le(11)),
        (dataframe['edad'].gt(11) & dataframe['edad'].le(18)),
        (dataframe['edad'].gt(18) & dataframe['edad'].le(26)),
        (dataframe['edad'].gt(26) & dataframe['edad'].le(59)),
        (dataframe['edad'].ge(60))], 
        ['primera infancia','niñez','adolescentes','adultos jovenes','adultos','adultos mayores'])
    return dataframe                                                                        

def getStudentsByAgeAndGender(startDate, endDate, category):
    df = getInscripcionAlumnosSubDf(startDate, endDate, category).groupby(
        ['grupo_edad', 'sexo'])['grupo_edad'].agg(['count']).reset_index()

    fig = px.bar(df, x=df['grupo_edad'], y=df['count'], color=df['sexo'], labels={
        "count": "Cantidad",
        "grupo_edad": "Grupo Edad",
        "sexo": "Género"
    }, title="Distribución de estudiantes por edad y género <br>{} - {}".format(startDate.date(), endDate.date()))
    fig.update_xaxes(categoryorder='array', categoryarray=[
                     'primera infancia', 'niñez', 'adolescentes', 'adultos jovenes', 'adultos', 'adultos mayores'])
    fig.update_layout(title_x=0.5)
    return fig

def getCoursesByEducationalStage(startDate, endDate, category):
    
    inscripcion_alumnos = getInscripcionAlumnosSubDf(startDate, endDate, category)
    inscripcion_grupos = pd.merge(inscripcion_alumnos, db.df_grupos, on='nmsec_grupo')
    inscripcion_grupos = inscripcion_grupos.groupby(['dsescolaridad', 'dsgrupo','categoria']).size().reset_index()
    inscripcion_grupos = inscripcion_grupos.rename(columns={0: 'Estudiantes'})
    inscripcion_grupos = inscripcion_grupos.sort_values(by='Estudiantes', ascending=False)
    new_df = inscripcion_grupos[0:80]
    new_df['dsgrupo'] = new_df['dsgrupo'].apply(lambda x: (x.lower()).title())

    fig = px.bar(new_df, x=new_df['dsgrupo'], y=new_df['Estudiantes'], color=new_df['categoria'],barmode = 'group', animation_frame=new_df['dsescolaridad'], height=800,
                title="Cursos con mayor demanda <br>{} - {}.".format(startDate, endDate))
    fig.update_xaxes(
            tickangle = -45,
            title_font = {"size": 20},
            title_standoff = 25)
    fig.update_layout(xaxis_title="Escolaridad",yaxis_title="Número de Estudiantes")
    fig['layout']['sliders'][0]['pad']=dict(t=150, l=-80, r=50)
    fig["layout"].pop("updatemenus")
    
    return fig

def getStudentsByMunicipality(startDate, endDate, category):

    df = getInscripcionAlumnosSubDf(startDate, endDate, category)
    df = df.drop_duplicates(subset ="nmsec_dni_x", keep = False)

    cantidad_null = len(df[df['nmsec_municipio'].isnull()])

    df = df[df['nmsec_municipio'].notnull()]
    df['nmsec_municipio'] = df['nmsec_municipio'].astype(int)
    df['month_year'] = df['fepago'].dt.strftime('%Y-%m')
    df_size = df.groupby(['nmsec_municipio']).size().reset_index().sort_values(by=0, ascending=False)[0:15]
    top5 = list(df_size['nmsec_municipio'][0:5])
    df = df.groupby(['nmsec_municipio', 'categoria']).size().reset_index().sort_values(by=0, ascending=False)
    df = df[df['nmsec_municipio'].isin(top5)]

    df = pd.merge(df, db.df_municipios[['nmsec_municipio', 'dsmunicipio']], on='nmsec_municipio')
    df = df.rename(columns={0: 'estudiantes'})
    df['dsmunicipio'] = df['dsmunicipio'].apply(lambda x: (x.lower()).title())

    fig = px.bar(df, x='dsmunicipio', y='estudiantes', color='categoria', title='Distribución de estudiantes por municipio <br>{} - {}'.format(startDate.date(), endDate.date()), log_y=True)
    fig.update_layout(xaxis_title="Municipio <br>{} estudiantes sin registro de municipio".format(cantidad_null), yaxis_title="Número de Estudiantes", title_x=0.5)
    return fig

def getEnrollmentByTrainingCenter(startDate, endDate):
    df = getInscripcionAlumnosSubDf(startDate, endDate, '')
    df = pd.merge(df, db.df_grupos[['nmsec_grupo', 'dsgrupo','nmsec_sede', 'nmsec_curso']], on='nmsec_grupo')
    df = pd.merge(df, db.df_cursos[['nmsec_curso', 'dscurso']] , on='nmsec_curso')
    df = pd.merge(df, db.df_sedes[['nmsec_sede', 'dsnombre_sede']], on='nmsec_sede')
    df = df.groupby(['dsnombre_sede', 'fepago']).size().reset_index()
    df = df.rename(columns={0: 'inscripciones'})

    df['dsnombre_sede'] = df['dsnombre_sede'].apply(lambda x: (x.lower()).title())
    df_sum = df.groupby(['dsnombre_sede']).sum().reset_index().sort_values(by='inscripciones', ascending=False)
    top5 = list(df_sum['dsnombre_sede'][0:5])

    fig = go.Figure()
    for sede in top5:
        fig.add_trace(go.Scatter(x=df[df['dsnombre_sede']==sede]['fepago'], y=df[df['dsnombre_sede']==sede]['inscripciones'],
                            mode='lines+markers',
                            name=sede))
    fig.update_layout(height=800, xaxis_title="Fecha", title_text='Inscripciones por sede - Top 5 <br>{} - {}'.format(startDate.date(), endDate.date()), 
    yaxis_title="Número de inscripciones", title_x=0.5, 
    legend=dict(orientation="h",yanchor="top",y=1.0,xanchor="right",x=1.0, font=dict(
            family="Courier",
            size=11,
            color="black"
        ),))    
    return fig

def getTopCoursesByAge(startYear, endYear, category):
    df = getInscripcionAlumnosSubDf(startYear, endYear, category)
    df = pd.merge(df, db.df_grupos, on='nmsec_grupo')
    df = pd.merge(df, db.df_cursos, on='nmsec_curso')
    df = df.groupby(['grupo_edad', 'dscurso']).size().reset_index()
    df = df.rename(columns={0: 'estudiantes'})
    df = df.sort_values(by='estudiantes', ascending=False)
    df_sub = df[df['grupo_edad'] == 'adultos'][0:10]

    edad_val = ['adultos mayores', 'adolescentes', 'niñez','primera infancia', 'adultos jovenes']
    
    for var in edad_val:
        sub_grup = df[df['grupo_edad'] == var][0:10]
        df_sub = df_sub.append(sub_grup)
    
    df_sub['dscurso'] = df_sub['dscurso'].apply(lambda x: (x.lower()).title())     
    fig = px.bar(df_sub, x='dscurso', y='estudiantes', animation_frame='grupo_edad', height=800,
                title="Cursos con mayor demanda <br> {} - {}".format(startYear.date(), endYear.date()))  
    fig.update_xaxes(title_font = {"size": 16},title_standoff = 25, tickangle = -45,)
    fig['layout']['sliders'][0]['pad']=dict(t=200, l=-70)
    fig["layout"].pop("updatemenus")
    fig.update_layout(xaxis_title="Cursos", yaxis_title="Número de Estudiantes", title_x=0.5)
    # fig.update_traces(marker_color='rgb(158,202,225)', marker_line_color='rgb(8,48,107)',
    #              marker_line_width=1.5, opacity=0.6)
    return fig


def getTopGroupsByAge(startYear, endYear, category):
    inscripcion_alumnos = getInscripcionAlumnosSubDf(startYear, endYear, category)
    inscripcion_grupos = pd.merge(inscripcion_alumnos, db.df_grupos, on='nmsec_grupo')
    inscripcion_grupos = inscripcion_grupos.groupby(['grupo_edad', 'dsgrupo']).size().reset_index()
    inscripcion_grupos = inscripcion_grupos.rename(columns={0: 'estudiantes'})
    inscripcion_grupos = inscripcion_grupos.sort_values(by='estudiantes', ascending=False)
    inscripcion_grupos_sub = inscripcion_grupos[inscripcion_grupos['grupo_edad'] == 'adultos'][0:10]
    
    edad_val = ['adultos mayores', 'adolescentes', 'niñez','primera infancia', 'adultos jovenes']
    
    for var in edad_val:
        sub_grup = inscripcion_grupos[inscripcion_grupos['grupo_edad'] == var][0:10]
        inscripcion_grupos_sub = inscripcion_grupos_sub.append(sub_grup)
    
    inscripcion_grupos_sub['dsgrupo'] = inscripcion_grupos_sub['dsgrupo'].apply(lambda x: (x.lower()).title())
    
    fig = px.bar(inscripcion_grupos_sub, x='dsgrupo', y='estudiantes', animation_frame='grupo_edad', height=800,
                title="Grupos con mayor demanda <br>{} - {}".format(startYear.date(), endYear.date()),)
    fig.update_xaxes(title_font = {"size": 16},title_standoff = 25, tickangle = -45,)
    #fig.update_traces(marker_color='rgb(158,202,225)', marker_line_color='rgb(8,48,107)',
    #                  marker_line_width=1.5, opacity=0.6)
    fig['layout']['sliders'][0]['pad']=dict(t=160, l=-70)
    fig["layout"].pop("updatemenus")
    fig.update_layout(xaxis_title="Grupo", yaxis_title="Número de Estudiantes", title_x=0.5)
    return fig

#######
#Cursos
#######

def getCategoryByCourses(startDate, endDate, searchId):
    alumnos = getInscripcionAlumnosSubDf(startDate, endDate, '')[['nmsec_dni_alumno', 'fepago', 'categoria', 'nmsec_grupo']]
    df = pd.merge(alumnos, db.df_grupos[['nmsec_grupo', 'dsgrupo', 'nmsec_curso']], on='nmsec_grupo')
    cursos = db.df_cursos[db.df_cursos['nmsec_curso'].isin(searchId)]
    df = pd.merge(df, cursos, on='nmsec_curso')
    df['categoria'] = df['categoria'].apply(lambda x: 'D' if x is None else x)
    df = df.groupby(['dscurso','categoria']).size().reset_index()
    df = df.rename(columns={0: 'estudiantes'})
    df['dscurso'] = df['dscurso'].apply(lambda x: (x.lower()).title())

    fig = px.bar(df, x='dscurso', y='estudiantes', color='categoria', title='Distribución de estudiantes por categoría <br>{} - {}'.format(startDate, endDate))
    fig.update_layout(xaxis_title="Curso", yaxis_title="Número de Estudiantes", title_x=0.5)
    return fig

def getCategoryByProgram(startDate, endDate, searchId):
    alumnos = getInscripcionAlumnosSubDf(startDate, endDate, '')[['nmsec_dni_alumno', 'fepago', 'categoria', 'nmsec_grupo']]
    df = pd.merge(alumnos, db.df_grupos[['nmsec_grupo', 'dsgrupo', 'nmsec_curso']], on='nmsec_grupo')
    df = pd.merge(df, db.df_cursos, on='nmsec_curso')
    programas = db.df_programas[db.df_programas['nmsec_programa'].isin(searchId)]
    df = pd.merge(df, programas, on='nmsec_programa')
    df['categoria'] = df['categoria'].apply(lambda x: 'D' if x is None else x)
    df = df.groupby(['dsprograma','categoria']).size().reset_index()
    df = df.rename(columns={0: 'estudiantes'})
    df['dsprograma'] = df['dsprograma'].apply(lambda x: (x.lower()).title())
    
    fig = px.bar(df, x='dsprograma', y='estudiantes', color='categoria', title='Distribución de estudiantes por categoría <br>{} - {}'.format(startDate, endDate))
    fig.update_layout(xaxis_title="Programa", yaxis_title="Número de Estudiantes", title_x=0.5)
    return fig

def getCategoryByArea(startDate, endDate, searchId):
    alumnos = getInscripcionAlumnosSubDf(startDate, endDate, '')[['nmsec_dni_alumno', 'fepago', 'categoria', 'nmsec_grupo']]
    df = pd.merge(alumnos, db.df_grupos[['nmsec_grupo', 'dsgrupo', 'nmsec_curso']], on='nmsec_grupo')
    df = pd.merge(df, db.df_cursos, on='nmsec_curso')
    df = pd.merge(df, db.df_programas, on='nmsec_programa')
    
    areas = db.df_areas[db.df_areas['nmsec_area'].isin(searchId)]
    df = pd.merge(df, areas, on='nmsec_area')
    df['categoria'] = df['categoria'].apply(lambda x: 'D' if x is None else x)
    df = df.groupby(['dsarea','categoria']).size().reset_index()
    df = df.rename(columns={0: 'estudiantes'})
    df['dsarea'] = df['dsarea'].apply(lambda x: (x.lower()).title())
    
    fig = px.bar(df, x='dsarea', y='estudiantes', color='categoria', title='Distribución de estudiantes por categoría <br>{} - {}'.format(startDate, endDate))
    fig.update_layout(xaxis_title="Area", yaxis_title="Número de Estudiantes", title_x=0.5)
    return fig

def getAgeByCourses(startDate, endDate, searchId):
    df = getInscripcionAlumnosSubDf(startDate, endDate, '').sort_values(by='fepago', ascending=True)
    df = pd.merge(df, db.df_grupos[['nmsec_grupo', 'nmsec_curso']], on='nmsec_grupo')

    cursos = db.df_cursos[db.df_cursos['nmsec_curso'].isin(searchId)]
    df = pd.merge(df, cursos, on='nmsec_curso')
    df['grupo_edad'] = df['grupo_edad'].apply(lambda x: (x.lower()).title())
    lista_grupos = list(df['grupo_edad'].unique())
     
    fig = go.Figure()
    for grupo in lista_grupos:
        fig.add_trace(go.Histogram(name=grupo,nbinsx=20, x=df[df['grupo_edad']==grupo]['edad']))
    
    fig.update_layout(barmode='stack')
    fig.update_traces(opacity=0.40, xbins=dict(start=0, end=100, size=2))
    fig.update_layout(barmode='overlay')
    fig.update_layout(xaxis_title="Edad", title_text='Distribución por Edad<br>{} - {}'.format(startDate, endDate), yaxis_title="Número de Estudiantes", title_x=0.5)
    return fig

def getAgeByProgram(startDate, endDate, searchId):
    df = getInscripcionAlumnosSubDf(startDate, endDate, '').sort_values(by='fepago', ascending=True)
    df = pd.merge(df, db.df_grupos[['nmsec_grupo', 'nmsec_curso']], on='nmsec_grupo')
    df = pd.merge(df, db.df_cursos, on='nmsec_curso')
    programas = db.df_programas[db.df_programas['nmsec_programa'].isin(searchId)]
    df = pd.merge(df, programas, on='nmsec_programa')
    df['grupo_edad'] = df['grupo_edad'].apply(lambda x: (x.lower()).title())
    lista_grupos = list(df['grupo_edad'].unique())
     
    fig = go.Figure()
    for grupo in lista_grupos:
        fig.add_trace(go.Histogram(name=grupo,nbinsx=20, x=df[df['grupo_edad']==grupo]['edad']))
    fig.update_layout(barmode='stack')
    fig.update_traces(opacity=0.40, xbins=dict(start=0, end=100, size=2))
    fig.update_layout(barmode='overlay')
    fig.update_layout(xaxis_title="Edad", title_text='Distribución por Edad<br>{} - {}'.format(startDate, endDate), yaxis_title="Número de Estudiantes", title_x=0.5)
    return fig

def getAgeByArea(startDate, endDate, searchId):
    df = getInscripcionAlumnosSubDf(startDate, endDate, '').sort_values(by='fepago', ascending=True)
    df = pd.merge(df, db.df_grupos[['nmsec_grupo', 'nmsec_curso']], on='nmsec_grupo')
    df = pd.merge(df, db.df_cursos, on='nmsec_curso')    
    df = pd.merge(df, db.df_programas, on='nmsec_programa')    
    areas = db.df_areas[db.df_areas['nmsec_area'].isin(searchId)]
    df = pd.merge(df, areas, on='nmsec_area')    
    df['grupo_edad'] = df['grupo_edad'].apply(lambda x: (x.lower()).title())
    lista_grupos = list(df['grupo_edad'].unique())
     
    fig = go.Figure()
    for grupo in lista_grupos:
        fig.add_trace(go.Histogram(name=grupo,nbinsx=20, x=df[df['grupo_edad']==grupo]['edad']))
    fig.update_layout(barmode='stack')
    fig.update_traces(opacity=0.40, xbins=dict(start=0, end=100, size=2))
    fig.update_layout(barmode='overlay')
    fig.update_layout(xaxis_title="Edad", title_text='Distribución por Edad<br>{} - {}'.format(startDate, endDate), yaxis_title="Número de Estudiantes", title_x=0.5)
    return fig

def getRevenueByCourses(startDate, endDate, searchId):
    df = getInscripcionAlumnosSubDf(startDate, endDate, '')[['fepago', 'categoria', 'valor', 'nmsec_grupo']].sort_values(by='fepago', ascending=True)
    df = pd.merge(df, db.df_grupos[['nmsec_grupo', 'dsgrupo', 'nmsec_curso']], on='nmsec_grupo')
    df = pd.merge(df, db.df_cursos, on='nmsec_curso')
    df = df[df['nmsec_curso'].isin(searchId)]
    df = df.groupby(['dscurso', 'categoria'])['dscurso','valor', ].sum().sort_values(by='valor', ascending=False).reset_index()
    
    df.sort_values(by='valor', ascending=False)
    df['dscurso'] = df['dscurso'].apply(lambda x: (x.lower()).title())

    fig = px.bar(df, x="valor", y="dscurso", color='categoria', orientation='h',             
                 height=400,
                 title='Ingresos por cursos <br>{} - {}'.format(startDate, endDate))
    fig.update_layout(title_x=0.5)
    fig.update_layout(xaxis_title="Valor", yaxis_title="Curso", title_x=0.5)
    # fig.update_yaxes(tickangle=-90)
    return fig

def getRevenueByProgram(startDate, endDate, searchId):
    df = getInscripcionAlumnosSubDf(startDate, endDate, '')[['fepago', 'categoria', 'valor', 'nmsec_grupo']].sort_values(by='fepago', ascending=True)
    df = pd.merge(df, db.df_grupos[['nmsec_grupo', 'dsgrupo', 'nmsec_curso']], on='nmsec_grupo')
    df = pd.merge(df, db.df_cursos, on='nmsec_curso')
    df = pd.merge(df, db.df_programas, on='nmsec_programa')
    df = df[df['nmsec_programa'].isin(searchId)]
    df = df.groupby(['dsprograma', 'categoria'])['dsprograma','valor', ].sum().sort_values(by='valor', ascending=False).reset_index()
    
    df.sort_values(by='valor', ascending=False)
    df['dsprograma'] = df['dsprograma'].apply(lambda x: (x.lower()).title())

    fig = px.bar(df, x="valor", y="dsprograma", color='categoria', orientation='h',             
                 height=400,
                 title='Ingresos por programas <br>{} - {}'.format(startDate, endDate))
    fig.update_layout(xaxis_title="Valor", yaxis_title="Programa", title_x=0.5)
    # fig.update_yaxes(tickangle=-90)
    return fig

def getRevenueByArea(startDate, endDate, searchId):
    df = getInscripcionAlumnosSubDf(startDate, endDate, '')[['fepago', 'categoria', 'valor', 'nmsec_grupo']].sort_values(by='fepago', ascending=True)
    df = pd.merge(df, db.df_grupos[['nmsec_grupo', 'dsgrupo', 'nmsec_curso']], on='nmsec_grupo')
    df = pd.merge(df, db.df_cursos, on='nmsec_curso')
    df = pd.merge(df, db.df_programas, on='nmsec_programa')
    df = pd.merge(df, db.df_areas, on='nmsec_area')
    df = df[df['nmsec_area'].isin(searchId)]
    df = df.groupby(['dsarea', 'categoria'])['dsarea','valor', ].sum().sort_values(by='valor', ascending=False).reset_index()
    
    df.sort_values(by='valor', ascending=False)
    df['dsarea'] = df['dsarea'].apply(lambda x: (x.lower()).title())

    fig = px.bar(df, x="valor", y="dsarea", color='categoria', orientation='h',             
                 height=400,
                 title='Ingresos por areas <br>{} - {}'.format(startDate, endDate))
                 
    # fig.update_yaxes(tickangle=-90)
    fig.update_layout(xaxis_title="Valor", yaxis_title="Area", title_x=0.5)
    return fig