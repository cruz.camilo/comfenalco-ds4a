from lists import *
import dash_core_components as dcc
import dash_bootstrap_components as dbc

########################
# Student Form
########################



#############################################################################
# Componentes
#############################################################################
#Componentes para el formato cero

category_op = dbc.FormGroup(
    [
        dbc.Label('Categoría:', width=4, className='label-horizontal'),
        dbc.Col(
            dcc.Dropdown(
                options=[
                    {'label': 'A', 'value': 'A'},
                    {'label': 'B', 'value': 'B'},
                    {'label': 'C', 'value': 'C'},
                    {'label': 'D', 'value': 'D'},
                    {'label': 'Todas', 'value': 'T'},
                ],
                id='category_op',
                multi=True,
                placeholder='Selecciona la categoría de afiliación',
                className='drop',
            ),
        )
    ],
    row=True,
)


student_start_date_op = dbc.FormGroup(
    [
        dbc.Label('Desde:', width=4, className='label-horizontal'),
        dbc.Col(
            dbc.Input(type="date", className="time_input", id='student_start_date_op', min="2013-05-06", max="2020-08-11"),
            width=8,
        ),
    ],
    row=True,
)


student_end_date_op = dbc.FormGroup(
    [
        dbc.Label('Hasta:', width=4, className='label-horizontal'),
        dbc.Col(
            dbc.Input(type="date", className="time_input",  id='student_end_date_op', min="2013-05-06", max="2020-08-11"),
            width=8,
        ),
    ],
    row=True,
)


submit_op = dbc.FormGroup(
    [
        dbc.Button(
            "Buscar",
            className="buttonS1",
            id='search_student_1_op',
            n_clicks=0,
            block=True,
            disabled=True
        )
    ]
)

#Componentes pra el primer formato 

category = dbc.FormGroup(
    [
        dbc.Label('Categoría:', width=4, className='label-horizontal'),
        dbc.Col(
            dcc.Dropdown(
                options=[
                    {'label': 'A', 'value': 'A'},
                    {'label': 'B', 'value': 'B'},
                    {'label': 'C', 'value': 'C'},
                    {'label': 'D', 'value': 'D'},
                    {'label': 'Todas', 'value': 'T'},
                ],
                id='category',
                multi=True,
                placeholder='Selecciona la categoría de afiliación',
                className='drop',
            ),
        )
    ],
    row=True,
)

student_start_date = dbc.FormGroup(
    [
        dbc.Label('Desde:', width=4, className='label-horizontal'),
        dbc.Col(
            dbc.Input(type="date", className="time_input", id='student_start_date', min="2013-05-06", max="2020-08-11"),
            width=8,
        ),
    ],
    row=True,
)

student_end_date = dbc.FormGroup(
    [
        dbc.Label('Hasta:', width=4, className='label-horizontal'),
        dbc.Col(
            dbc.Input(type="date", className="time_input",  id='student_end_date', min="2013-05-06", max="2020-08-11"),
            width=8,
        ),
    ],
    row=True,
)

submit = dbc.FormGroup(
    [
        dbc.Button(
            "Buscar",
            className="buttonS1",
            id='search_student_1',
            n_clicks=0,
            block=True,
            disabled=True,
          )
    ]
)

#Componentes para el segundo formato

category_op2 = dbc.FormGroup(
    [
        dbc.Label('Categoría:', width=4, className='label-horizontal'),
        dbc.Col(
            dcc.Dropdown(
                options=[
                    {'label': 'A', 'value': 'A'},
                    {'label': 'B', 'value': 'B'},
                    {'label': 'C', 'value': 'C'},
                    {'label': 'D', 'value': 'D'},
                    {'label': 'Todas', 'value': 'T'},
                ],
                id='category_op2',
                multi=True,
                placeholder='Selecciona la categoría de afiliación',
                className='drop',
            ),
        )
    ],
    row=True,
)


student_start_date_op2 = dbc.FormGroup(
    [
        dbc.Label('Desde:', width=4, className='label-horizontal'),
        dbc.Col(
            dbc.Input(type="date", className="time_input", id='student_start_date_op2', min="2013-05-06", max="2020-08-11"),
            width=8,
        ),
    ],
    row=True,
)


student_end_date_op2 = dbc.FormGroup(
    [
        dbc.Label('Hasta:', width=4, className='label-horizontal'),
        dbc.Col(
            dbc.Input(type="date", className="time_input",  id='student_end_date_op2', min="2013-05-06", max="2020-08-11"),
            width=8,
        ),
    ],
    row=True,
)


submit_op2 = dbc.FormGroup(
    [
        dbc.Button(
            "Buscar",
            className="buttonS1",
            id='search_student_1_op2',
            n_clicks=0,
            block=True,
            disabled=True
        )
    ]
)
#############################################################################
# FORMATOS
#############################################################################

#Formato 0
student_form_op = dbc.Form(
        [        
            student_start_date_op,
            student_end_date_op,
            category_op, 
            submit_op
        ],
        id='student_form_op'

)

#Formato 1
student_form = dbc.Form(
        [        
            student_start_date,
            student_end_date,
            category, 
            submit
        ],
        id='student_form'

)

#Formato 2
student_form_op2 = dbc.Form(
        [        
            student_start_date_op2,
            student_end_date_op2,
            category_op2, 
            submit_op2
        ],
        id='student_form_op2'

)
