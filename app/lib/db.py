import pandas as pd
import re
import io
from unicodedata import normalize
from sqlalchemy import create_engine
import numpy as np
import plotly.express as px
import time
from datetime import datetime
import psycopg2
from app import app

host = 'db-comfenalco55-instance.c4bdw8ktcduw.us-east-2.rds.amazonaws.com'
port = 5432
user = 'postgres'
password = 'comfenalco'
database = 'comfenalco'

comfenalco_DB = create_engine(f'postgresql://{user}:{password}@{host}:{port}/{database}')
conn = comfenalco_DB.raw_connection()
cur = conn.cursor()

df_areas = pd.read_sql('SELECT * FROM df_areas_reduct ORDER BY nmsec_area ASC', comfenalco_DB)
df_programas = pd.read_sql('SELECT * FROM df_programas', comfenalco_DB)
df_inscripcion =  pd.read_sql('SELECT * FROM df_inscripcion', comfenalco_DB)
df_inscripcion['fepago'] = pd.to_datetime(df_inscripcion['fepago'])

def getGroupsDf():
    df_grupos = pd.read_sql('SELECT * FROM df_grupos', comfenalco_DB)
    df_grupos['feinicio_curso'] = pd.to_datetime(df_grupos['feinicio_curso'])
    grupos_en_inscripcion = list(df_inscripcion['nmsec_grupo'])
    df_grupos_reducido = df_grupos[df_grupos['nmsec_grupo'].isin(grupos_en_inscripcion)]
    return df_grupos_reducido

df_grupos = getGroupsDf()

def getCoursesFiltredDf():
    df_cursos = pd.read_sql('SELECT * FROM df_cursos', comfenalco_DB)
    grupo_nmsec_list = list(df_grupos['nmsec_curso'])
    courses_filtred_df = df_cursos[df_cursos['nmsec_curso'].isin(grupo_nmsec_list)]
    return courses_filtred_df

df_cursos = getCoursesFiltredDf()

def getCoursesList(startDate, endDate):
    inscripcion = df_inscripcion[(df_inscripcion['fepago'] >= startDate) 
    & (df_inscripcion['fepago'] < endDate)][['nmsec_inscripcion', 'nmsec_grupo', 'fepago']]
    df = pd.merge(inscripcion, df_grupos[['nmsec_grupo', 'dsgrupo', 'nmsec_curso']], on='nmsec_grupo')
    cursos = df.nmsec_curso.unique()
    df = df_cursos[df_cursos['nmsec_curso'].isin(cursos)][['nmsec_curso', 'dscurso_2', 'nmsec_programa']]
    return df

def getProgramsList(startDate, endDate):
    courses_list = getCoursesList(startDate, endDate)
    programas_en_cursos = list(courses_list['nmsec_programa'].unique())
    df_programas_list = df_programas[df_programas['nmsec_programa'].isin(programas_en_cursos)]
    return df_programas_list

def getAreasList(starDate, endDate):
    programs_list = getProgramsList(starDate, endDate)
    areas_en_programas = list(programs_list['nmsec_area'].unique())
    df_areas_list = df_areas[df_areas['nmsec_area'].isin(areas_en_programas)] 
    return df_areas_list

df_municipios = pd.read_sql(
    '''SELECT nmmunicipio, dsmunicipio 
    FROM df_municipios WHERE nmdepartamento = 5 
    AND nmsec_municipio != 1107 
    ORDER BY nmmunicipio ASC''', comfenalco_DB)

df_alumnos = pd.read_sql('SELECT * FROM df_alumnos', comfenalco_DB)
df_alumnos_empl_esc_mun= pd.read_sql('SELECT * FROM df_alumnos_empl_esc_mun', comfenalco_DB)
df_cursos_edu_pro_are =pd.read_sql('SELECT * FROM df_cursos_edu_pro_are', comfenalco_DB)
df_alumnos_escolaridad = pd.read_sql('SELECT * FROM df_alumnos_escolaridad',comfenalco_DB)
df_grupos = pd.read_sql('SELECT * FROM df_grupos',comfenalco_DB)
df_grupos_cob = pd.read_sql('SELECT * FROM df_grupos_cob', comfenalco_DB)
df_escolaridad = pd.read_sql('SELECT DISTINCT dsescolaridad, nmsec_escolaridad FROM df_alumnos_escolaridad order by nmsec_escolaridad asc'
,comfenalco_DB)
df_municipios = pd.read_sql('SELECT nmmunicipio, dsmunicipio, nmsec_municipio FROM df_municipios WHERE nmdepartamento = 5',comfenalco_DB)
municipios_list = [(municipio.lower()).title() for municipio in list(df_municipios['dsmunicipio'])]
df_programas['dsprograma'] = df_programas['dsprograma'].apply(lambda x: (x.lower()).title())
df_sedes = pd.read_sql('SELECT * FROM df_sedes',comfenalco_DB)