import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html


# Tarjetas para los 6 cursos más recomendados
card_content1 = [
    dbc.CardHeader(id='program-card1'),
    dbc.CardBody(
        [
            html.H5(id='course-card1', className="card-title"),
            html.P(id='accuracy-card1', className="accuracy"), 
        ],
    ), 
]

card_content2 = [
    dbc.CardHeader(id='program-card2'),
    dbc.CardBody(
        [
            html.H5(id='course-card2', className="card-title"),
            html.P(id='accuracy-card2', className="accuracy"), 
        ],
    ), 
]

card_content3 = [
    dbc.CardHeader(id='program-card3'),
    dbc.CardBody(
        [
            html.H5(id='course-card3', className="card-title"),
            html.P(id='accuracy-card3', className="accuracy"), 
        ],
    ), 
]

card_content4 = [
    dbc.CardHeader(id='program-card4'),
    dbc.CardBody(
        [
            html.H5(id='course-card4', className="card-title"),
            html.P(id='accuracy-card4', className="accuracy"), 
        ],
    ), 
]

card_content5 = [
    dbc.CardHeader(id='program-card5'),
    dbc.CardBody(
        [
            html.H5(id='course-card5', className="card-title"),
            html.P(id='accuracy-card5', className="accuracy"), 
        ],
    ), 
]

card_content6 = [
    dbc.CardHeader(id='program-card6'),
    dbc.CardBody(
        [
            html.H5(id='course-card6', className="card-title"),
            html.P(id='accuracy-card6', className="accuracy"), 
        ],
    ), 
]

# Filas
row_1 = dbc.Row(
    [
        dbc.Col(dbc.Card(card_content1, id='card1', color="secondary", style={'display': 'none'}, outline=True), width={"size": 4, "offset": 0}),
        dbc.Col(dbc.Card(card_content2, id='card2', color="secondary", style={'display': 'none'}, outline=True), width={"size": 4, "offset": 0}),
        dbc.Col(dbc.Card(card_content3, id='card3', color="secondary", style={'display': 'none'}, outline=True), width={"size": 4, "offset": 0}),
    ],
)

row_2 = dbc.Row(
    [
        dbc.Col(dbc.Card(card_content4, id='card4', color="secondary", style={'display': 'none'}, outline=True), width={"size": 4, "offset": 0}),
        dbc.Col(dbc.Card(card_content5, id='card5', color="secondary", style={'display': 'none'}, outline=True), width={"size": 4, "offset": 0}),
        dbc.Col(dbc.Card(card_content6, id='card6', color="secondary", style={'display': 'none'}, outline=True), width={"size": 4, "offset": 0}),
    ],
)