import pandas as pd
from lib import db, plots_methods
from lib.plots_methods import addGrupoEdadColumn
import numpy as np
import fuzzymatcher
import pickle
import re

left_on = ['sexo', 'edad', 'grupo_edad_pago', 'dsmunicipio','categoria', 'dsescolaridad','virtual'] #cols of our database
right_on = ['sexo', 'edad','grupo_edad', 'dsmunicipio','categoria', 'dsescolaridad','virtual'] #cols of input database

def recommendCourses(gender, age, town, category, educational_stage, course_modality):
    if (1 in course_modality) and (2 in course_modality):        
        df1 = getInputDataframe(1, gender, age, town, category, educational_stage, 'PRESENCIAL')
        print(df1)
        df1_result = matchProfiles(df1, False, 3)
        print(df1_result)
        df2 = df1.copy()
        df2['virtual']='VIRTUAL'
        print(df2)
        df2_result = matchProfiles(df2, True, 3)
        print(df2_result)
        df = df1_result.append(df2_result)
        return df
    if 1 in course_modality:        
        course_modality = 'PRESENCIAL'
        df = getInputDataframe(1, gender, age, town, category, educational_stage, course_modality)        
        return matchProfiles(df, False, 6)
    
    if 2 in course_modality:
        course_modality = 'VIRTUAL'
        df = getInputDataframe(1, gender, age, town, category, educational_stage, course_modality)
        return matchProfiles(df, True, 3)

#Resulting df from this method is stored in reference_matching_df file
def getMatchingDf():
    # Merge
    df = pd.merge(db.df_inscripcion, db.df_grupos_cob, how='inner', on=['nmsec_grupo'])
    df = pd.merge(df, db.df_cursos_edu_pro_are,how='inner', on=['nmsec_curso'])
    df = pd.merge(df, db.df_alumnos_empl_esc_mun, how='inner', left_on=['nmsec_dni_alumno'], right_on=['nmsec_dni'])

    # Cleaning
    df = df[df['cdestado_curso'] != 'ANULADO']
    df = df[~df.dscurso.isnull()]
    df = df[~df.dsmunicipio.isnull()]

    # Filter
    df['fepago'] = pd.to_datetime(df['fepago'])
    df = df[df['fepago'] >= '2016-01-01']
    df = df[~df['dsprograma'].str.contains('EDUCACION FORMAL')].reset_index(drop=True)

    #Null values on this column means not affiliated. Category D
    df['categoria'] = df['categoria'].apply(lambda x: 'D' if x is None else x)

    df_new=df.pivot_table(index=['nmsec_dni_x','sexo','edad_pago','grupo_edad_pago','dsmunicipio',
                                              'categoria','dsprograma','dscurso_2','dsescolaridad'],
                                       values='estado',aggfunc=lambda x: len(x)).reset_index()
    df_new['virtual']=np.where(df_new['dscurso_2'].str.contains('VIRTU'),'VIRTUAL','PRESENCIAL')
    df_new['edad_pago'] = df_new['edad_pago'].astype(int)
    df_new = df_new.rename(columns = {'edad_pago':'edad'})
    df_new['dscurso'] = df_new['dscurso_2'].apply(lambda x: re.sub(r'\([^)]*\)', '', x))   
    return df_new

def getInputDataframe(input_id, gender, age, town, category, educational_stage, course_modality):        
    df_values = {
        'id': [input_id],
        'sexo': [gender],
        'edad': [age],
        'dsmunicipio': [town],
        'categoria': [category],
        'dsescolaridad': [educational_stage],
        'virtual': [course_modality]
        }

    df = pd.DataFrame(df_values, columns = ['id','sexo', 'edad', 'dsmunicipio','categoria', 'dsescolaridad','virtual'])
    df['edad'] = df['edad'].astype(int)    
    df = addGrupoEdadColumn(df)
    print(df)
    return df

reference_df = pd.read_pickle('./reference_matching_df.pkl')
def matchProfiles(studentProfile, virtual, amount):
    df = getDfAfterBlocking(studentProfile)
    matched_results = fuzzymatcher.fuzzy_left_join(df,studentProfile,left_on,right_on,
                                                left_id_col='nmsec_dni_x',right_id_col='id')
    matched_nodup=matched_results.drop_duplicates(['id','dscurso']).reset_index(drop=True)
    matched_nodup=matched_nodup.sort_values(by=['id','best_match_score'], ascending=False)
    
    result = matched_nodup[matched_nodup['id']==1][0:amount].copy()    
    result = result.loc[:, ['id','best_match_score','dscurso', 'dsprograma',]]
    result['dsprograma'] = result['dsprograma'].apply(lambda x: (x.lower()).title())
    result['dscurso'] = result['dscurso'].apply(lambda x: (x.lower()).title())
    #Normalize match score based on
    #Min value after tests: -0.178
    #Max value: 0.150
    maxValue = 0.33

    result['best_match_score'] = result['best_match_score'].apply(lambda x: (round(float(x), 3) + 0.18)/maxValue)
    result['best_match_score'] = result['best_match_score'].apply(lambda x: (round(float(x), 2))*100)

    return result

def translateGroupAge(student_group_age):
    if student_group_age == 'primera infancia':
        return student_group_age.upper()
    if student_group_age == 'niñez':
        return 'INFANCIA'
    if student_group_age == 'adolescentes':
        return 'ADOLESCENCIA'
    if student_group_age == 'adultos jovenes':
        return 'JUVENTUD'
    if student_group_age == 'adultos':
        return 'ADULTEZ'
    if student_group_age == 'adultos mayores':
        return 'VEJEZ'

def getDfAfterBlocking(studentProfile):
    global reference_df
    
    input_group_age = translateGroupAge(studentProfile['grupo_edad'][0])
    input_category = studentProfile['categoria'][0]
    course_modality = studentProfile['virtual'][0]

    df = reference_df[reference_df['virtual']==course_modality]

    if (course_modality == 'VIRTUAL'):
        return df
    
    df = df[df['grupo_edad_pago']==input_group_age]

    if (input_group_age != 'ADULTEZ'):
        return df
    else:
        df = df[df['categoria']==input_category]
        return df