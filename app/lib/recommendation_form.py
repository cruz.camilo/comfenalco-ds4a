from lists import *
import dash_core_components as dcc
import dash_bootstrap_components as dbc

########################
# Recommendation Form
########################
# class recommendation:
#     def __init__(self):
#         self.gender = 'M'
#         self.age='19'
#         self.educational_stage='SECUNDARIA COMPLETA'
#         self.town='MEDELLIN'
#         self.category='B'
#         self.course_modality=[1]
# default = recommendation()

gender = dbc.FormGroup(
    [
        dbc.Label("Género:", md=4, className='label-horizontal'),
        dbc.Col(
            dcc.Dropdown(
                options=[
                    {'label': 'Masculino', 'value': 'M'},
                    {'label': 'Femenino', 'value': 'F'},
                ],
                id='gender',
                className='drop',
                placeholder='Selecciona el género',
                # value=default.gender
            ),
        ),
    ],
    row=True,
)

age = dbc.FormGroup(
    [
        dbc.Label("Edad:", md=4, className='label-horizontal'),
        dbc.Col(
            dbc.Input(
                type="number", min=0, max=100,
                id='age',
                className='drop',
                placeholder="Ingresa la edad",
                # value=default.age
            ),
        ),
    ],
    row=True,
)

educational_stage = dbc.FormGroup(
    [
        dbc.Label('Escolaridad:', md=4, className='label-horizontal'),
        dbc.Col(
            dcc.Dropdown(
                options=[
                    {'label': escolaridad[i], 'value': escolaridad_id[i]} for i in range(0, len(escolaridad))
                ],
                id="educational_stage",
                className='drop',
                placeholder='Nivel Educativo',
                # value=default.educational_stage
            ),
        )
    ],
    row=True,
)

location = dbc.FormGroup(
    [
        dbc.Label('Municipio:', md=4, className='label-horizontal'),
        dbc.Col(
            dcc.Dropdown(
                options=[
                    {'label': municipios[i], 'value': municipios_id[i]} for i in range(0, len(municipios))
                ],
                id='town',
                placeholder='Municipio',
                className='drop',
                # value=default.town
            ),
        )
    ],
    row=True,
)

category_recommendation = dbc.FormGroup(
    [
        dbc.Label('Categoria:', width=4, className='label-horizontal'),
        dbc.Col(
            dcc.Dropdown(
                options=[
                    {'label': 'A', 'value': 'A'},
                    {'label': 'B', 'value': 'B'},
                    {'label': 'C', 'value': 'C'},
                    {'label': 'D', 'value': 'D'},
                ],
                id='category_recommendation',                
                placeholder='Selecciona la categoría de afiliación',
                className='drop',
                # value=default.category
            ),
        )
    ],
    row=True,
)

course_modality = dbc.FormGroup(
    [
        dbc.Label('Modalidad de curso:', width=4,
                  className='label-horizontal'),
        dbc.Col(
            dcc.Dropdown(
                options=[
                    {'label': 'Presencial', 'value': 1},
                    {'label': 'Virtual', 'value': 2},
                ],
                id='course_modality',
                multi=True,
                placeholder='Selecciona la modalidad de curso',
                className='drop',
                # value=default.course_modality
            ),
        )
    ],
    row=True,
)

submit = dbc.FormGroup(
    [
        dbc.Button(
            "Recomendar",
            className="buttonS1",
            id='recommend_courses',
            n_clicks=0,
            block=True,
            disabled=True            
        )
    ]
)

recommendation_form = dbc.Form(
    [
        gender,
        age, 
        educational_stage, 
        location, 
        category_recommendation,
        course_modality,
        submit
    ],
    id='recommendation_form'
)

Form3 = dbc.Row(
    [
        dbc.Col(
            [
            dbc.Card([
                    dbc.CardHeader("Perfil del estudiante"),
                    dbc.CardBody(recommendation_form)
                ],color="light", outline=True
                ),
            ], 
        width={"size": 6, "offset": 3},
        ),
    ]
)