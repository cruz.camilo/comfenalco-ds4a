# Basics Requirements
import pathlib
import dash
from dash.dependencies import Input, Output
from dash.exceptions import PreventUpdate
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go
import plotly.express as px
import base64

# Dash Bootstrap Components
import dash_bootstrap_components as dbc

# Data
import math
import numpy as np
import datetime as dt
import pandas as pd
import json

# Recall app
from app import app


###########################################################
#
#           APP LAYOUT:
#
###########################################################

# LOAD THE DIFFERENT FILES
from lib import title, sidebar, body

# PLACE THE COMPONENTS IN THE LAYOUT
app.layout = html.Div(
    [
        sidebar.sidebar,
        title.title,
        body.body,        
        dcc.Location(id="url")
    ],
    className="ds4a-app",
)

###############################################
#
#           APP INTERACTIVITY:
#
###############################################

###############################################################
# Callbacks: Activar links para las paginas 
#################################################################
@app.callback(
    [Output(f"page-{i}-link", "active") for i in range(1, 4)],
    [Input("url", "pathname")],
)
def toggle_active_links(pathname):
    if pathname == "/":
        # Treat page 1 as the homepage / index
        return True, False, False
    return [pathname == f"/page-{i}" for i in range(1, 4)]


@app.callback(
    [
        Output("title-text", "children"),
        Output("sub-title", "children"),
        Output("page-content", "children"),
        ], 
    [Input("url", "pathname")])
def render_page_content(pathname):
    if pathname in ["/", "/page-1"]:
        return [
            "Visualización de Datos",
            "Estudiantes",
            body.body1
        ]

    elif pathname == "/page-2":
        return [
            "Visualización de Datos",
            "Cursos",
            body.body2
        ]

    elif pathname == "/page-3":
        return [
            "Recomendación de Cursos",
            "",
            body.body3
        ]
    # If the user tries to reach a different page, return a 404 message
    return dbc.Jumbotron(
        [
            html.H1("404: Not found", className="text-danger"),
            html.Hr(),
            html.P(f"The pathname {pathname} was not recognized..."),
        ]
    )


#############################################################
# __name__
#############################################################

if __name__ == "__main__":
    #   app.run_server(host="0.0.0.0", port="8053", debug=True)
    # app.run_server(host='0.0.0.0', debug=True)
    app.run_server(host='0.0.0.0', port=8080)